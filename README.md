# mipTemplate

## Purpose and functionality

This is a collection of tools and best practices to help you start your own model intercomparison project. 

The Rmd notebook `template.Rmd` showcases functionality provided by libraries such as `piamInterfaces`, `mip` and `quitte` that 
you can use to read in, maniputate, validate and plot data.

The Python script `python_scripts/read_data.py` demonstrates how to download data from an IIASA database using the Python API.

## Quickstart

In order to get a quick overview of what is offered so far without any setup, take a look at `html_example/template.html`.

## Prerequisites

Make sure you have the latest version of the libraries `piamInterfaces`, `mip` and `quitte` installed.

## Things covered so far

### Download submissions from an IIASA database

The best way to get submission data from an IIASA database is using their Python interface.
To get started, take a look at the script `python_scripts/read_data.py`. You need to install Python (tested with Python 3.10) 
and the [pyam package](https://pyam-iamc.readthedocs.io/en/stable/install.html).

### Read in and validate submissions in R

Take a look at the code in `template.Rmd` and `html_example/template.html` to see how to read in submission data downloaded from IIASA in R and do simple validation
checks, e.g. list missing variables and perform summation checks over regions and variables.

### Create plots from submissions

Take a look at the code in `template.Rmd` and `html_example/template.html` to see how to create simple comparison plots using custom line and bar plots in `ggplot` 
as well as more advanced plotting formats (similar to what you have in compareScenarios.pfd) using the `mip` library. 

There are also examples for how to calculate additional variables from the submissions using `quitte` and harmonize the model data with historical data using `mip`.

