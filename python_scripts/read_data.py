import pyam
from pandas import read_excel
from pathlib import Path

'''

This script downloads and the model and scenario files from an IIASA database and stores them
in a downloads folder on the same level as the script. If there are files in that folder already,
they are only updated if a newer version is available.

Check out requirements.txt for library dependencies. Make sure to run this script with Python 3.10.

More info in the pyam package providing the interface can be found here: 
https://pyam-iamc.readthedocs.io/en/stable/api/iiasa.html

'''


#### BEGIN Settings

# add your IIASA database credentials here
USERNAME = 'myUsername'
PASSWORD = 'myPassword'
DATABASE = 'myDatabase'

# list the models you want to include in your download
MODELS = ["REMIND 3.0", "PROMETHEUS V1", "MESSAGEix-Materials", "GEM-E3_v2020", "COFFEE 1.3", "IMAGE 3.2", "POLES NAVIGATE"]

# list the scenarios you want to include in your downoad 
VALID_SCENARIOS = ["NAV_Ind_1150", "NAV_Ind_650", "NAV_Ind_Comb1150", "NAV_Ind_Comb650", "NAV_Ind_Elec1150", "NAV_Ind_Elec650",
"NAV_Ind_NPi", "NAV_Ind_LMD1150", "NAV_Ind_LMD650"]	

#### END Settings 

pyam.iiasa.set_config(USERNAME, PASSWORD)
conn =  pyam.iiasa.Connection(DATABASE)

Path("download").mkdir(parents=True, exist_ok=True)
	
for m in MODELS:
	for s in VALID_SCENARIOS:
		df_s = conn.query(model=m, scenario = s)
		file_name = "download/"+m+"_"+s+".xlsx"
		try:
			f = read_excel(io=file_name, sheet_name="meta",engine='openpyxl')
			prev_version = f.at[0,'version']            
		except (KeyError, FileNotFoundError):
			print("No previous version of "+m+"_"+s+" found")
			prev_version = -1
			
		try:
			latest_version = df_s.meta.at[(m, s),"version"]
			
			if latest_version > prev_version:
				print("downloading new version "+str(latest_version)+" of "+file_name)
				df_s.to_excel(excel_writer=file_name)
			else:
				print("we already have latest version ("+str(latest_version)+") of "+file_name)        
		except KeyError:
			print(m+", "+s+" does not exist")
